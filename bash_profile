export GOPATH=/Users/yibin/gopath
export GOBIN=/usr/local/go/bin
export JAVA_HOME="$(/usr/libexec/java_home -v 1.8)"
export M2_HOME=/opt/apache-maven-3.5.0
export M2=$M2_HOME/bin
export MAVEN_OPTS='-Xms256m -Xmx512m'

export PATH=$M2:$PATH
export PATH=$PATH:$GOPATH/bin
export PATH=$PATH:/usr/local/sbin


export PATH="$(brew --prefix)/bin:$PATH"

export NODEPATH=/usr/local/bin/
export PATH=$PATH:$NODEPATH

export NPMPATH=/usr/local/bin
export PATH=$PATH:$NPMPATH

export ADBPATH=/Users/yibin/platform-tools
export PATH=$PATH:$ADBPATH

#export PS1="\u@\h:\W 💝 $ "
#export PS1="[\e[0;34m\]\u\[\e[m\]@\[\e[0;32m\]\h\[\e[m\] \[\e[0;36m\]\W\[\e[m\] \[\e[0;31m\]\t\[\e[m\]] 👉 $ "
alias tailf="tail -f "
alias flushdns="sh /Users/yibin/shell/flushdns.sh"
alias ll="ls -lh"
alias python="/System/Library/Frameworks/Python.framework/Versions/2.7/bin/python2.7"



export PATH=/usr/local/bin:/opt/apache-maven-3.3.9//bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/opt/local/bin:/usr/local/mysql-5.6.11-osx10.7-x86_64/lib:/usr/local/git/bin:/usr/local/go/bin:/Users/yibin/gopath/bin
export PATH="/usr/local/sbin:$PATH"



# where proxy
proxy(){
    export https_proxy=http://127.0.0.1:6152
    export http_proxy=http://127.0.0.1:6152
    export all_proxy=socks5://127.0.0.1:6153
    echo "Proxy On"
}


# where noproxy
noproxy(){
    unset https_proxy
    unset http_proxy
    unset all_proxy
    echo "Proxy Off"
}

#enables colorin the terminal bash shell export

export CLICOLOR=1

export TERM=xterm-256color
#sets up thecolor scheme for list export

LSCOLORS=gxfxcxdxbxegedabagacad

#sets up theprompt color (currently a green similar to linux terminal)

export PS1='[ಥ_ಥ @ \h]:\w ☛ $ '
